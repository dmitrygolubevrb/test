import Vue from "vue";
import Vuex from 'vuex';
import Auth from './modules/auth'
import Main from './modules/main'
import Currency from './modules/currency'
Vue.use(Vuex)

export default new Vuex.Store({
    modules: {
        Auth,
        Main,
        Currency
    },
})
