import Currency from "../../models/Currency";



const state = {
    currencies: null,
    currencyQuotations: null,
    currencyFilter: {
        valute_id: null,
        dates: []
    },
    countDaysSync: null
}

const getters = {
    CURRENCIES: () => state.currencies,
    CURRENCY_QUOTATIONS: () => state.currencyQuotations,
    CURRENCY_FILTER: () => state.currencyFilter,
    COUNT_DAYS_SYNC: () => state.countDaysSync
}

const mutations = {
    SET_CURRENCIES: (state, payload) => {
        state.currencies = payload
    },
    SET_CURRENCY_QUOTATIONS: (state, payload) => {
        state.currencyQuotations = payload
    },
    SET_CURRENCY_FILTER: (state, payload) => {
        state.currencyFilter = payload
    },
    SET_COUNT_DAYS_SYNC: (state, payload) => {
        state.countDaysSync = payload
    }
}

const actions = {
    SET_CURRENCIES: ({commit}, payload) => {
        commit('SET_CURRENCIES', payload)
    },
    SET_CURRENCY_QUOTATIONS: ({commit}, payload) => {
        commit('SET_CURRENCY_QUOTATIONS', payload)
    },
    GET_CURRENCIES: ({commit}, payload) => {
        new Currency().get()
            .then(res => {
                commit('SET_CURRENCIES', res.data)
            })
            .catch(error => {
                payload.notificator.error(error.response.data.message)
            })
    },
    GET_CURRENCY_QUOTATIONS: ({commit}, payload) => {
        let date = new Date()
        date.setDate(date.getDate() - 30)
        let monthAgoDate = date.toISOString().replace(/-/g, '/').slice(0, 10)
        axios.get(`/api/currencies/quotations?
        ${payload.filter && payload.filter.dates.length ? 'filter[from]=' + payload.filter.dates[0] + '&' + 'filter[to]=' + payload.filter.dates[1] :
            'filter[from]=' + monthAgoDate}
        ${payload.filter && payload.filter.valute_id ? '&filter[valute]=' + payload.filter.valute_id : ''}`)
            .then(res => {
                commit('SET_CURRENCY_QUOTATIONS', res.data.data)
                payload.filter ? payload.notificator.success('Данные отфильтрованы') : null
            })
            .catch(error => payload.notificator.error(error.response.data.message))

    },
    SET_CURRENCY_FILTER: ({commit}, payload) => {
        commit('SET_CURRENCY_FILTER', payload)
    },
    SET_COUNT_DAYS_SYNC: ({commit}, payload) => {
        commit('SET_COUNT_DAYS_SYNC', payload)
    },
    SYNC_CURRENCIES: ({commit, dispatch, getters}, payload) => {
        new Currency({
            'days_count': payload.countDaysSync
        }).save()
            .then(res => {
                dispatch('GET_CURRENCY_QUOTATIONS', {notificator: payload.notificator})
                payload.notificator.success('Данные успешно обновлены')
                commit('SET_CURRENCY_FILTER', {valute_id: null, dates: []})
                if(!getters.CURRENCIES.length) dispatch('GET_CURRENCIES')
            })
            .catch(error => {
                payload.notificator.error(error.response.data.message)
            })
    },
    DESTROY_CURRENSIES: ({commit}, payload) => {
        axios.delete('/api/currencies')
            .then(() => {
                commit('SET_CURRENCY_QUOTATIONS', null)
                commit('SET_CURRENCY_FILTER', {valute_id: null, dates: []})
                payload.notificator.success('Таблица успешно очищена')
            })
            .catch(error => payload.notificator.error(error.response.data.message))
    }
}

export default {
    state, mutations, getters, actions
}
