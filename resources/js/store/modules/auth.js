const state = {
    registrationFields: {
        name: null,
        email: null,
        password: null,
        password_confirmation: null
    },
    loginFields: {
        email: null,
        password: null
    },
    authUserInfo: {
        email: null,
        name: null,
    }
}

const getters = {
    REGISTRATION_FIELDS: () => state.registrationFields,
    LOGIN_FIELDS: () => state.loginFields,
    AUTH_USER_INFO: () => state.authUserInfo
}

const mutations = {
    SET_REGISTRATION_FIELDS: (state, registrationFields) => {
        state.registrationFields = registrationFields
    },
    SET_LOGIN_FIELDS: (state, loginFields) => {
        state.loginFields = loginFields
    },
    SET_AUTH_USER_INFO: (state, authUserInfo) => {
        state.authUserInfo = authUserInfo
    }
}

const actions = {

    REGISTRATION: ({}, data) => {
        console.log(data);
        axios.get('/sanctum/csrf-cookie').then(() => {
            axios.post('/register', data.registrationFields)
                .then(res => {
                    window.location.replace('/')
                })
                .catch(error => {
                    data.notification.error(error.response.data.message)
                })
        })

    },

    LOGIN: ({dispatch}, data) => {
        axios.get('/sanctum/csrf-cookie')
            .then(res => {
                axios.post('/login', data.loginFields)
                    .then(res => {
                        window.location.replace('/')
                    })
                    .catch(error => {
                        dispatch('SET_LOGIN_FIELDS', {email: null, password: null})
                        data.notification.error(error.response.data.message)
                    })
            })
    },
    LOGOUT: () => {
        axios.post('/logout').then(() => {
            window.location.replace('/auth/login')
        })
    },
    GET_AUTH_USER_INFO: ({commit}) => {
        axios.get('/api/user').then(res => {
            commit('SET_AUTH_USER_INFO', {
                email: res.data.data.email,
                name: res.data.data.name,
            })
        })
    }

}

export default {
    state, mutations, getters, actions
}
