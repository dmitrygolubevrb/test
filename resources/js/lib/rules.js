export default function (formData = null) {
    let validatePasswordConfirmed = (rule, value, callback) => {
        if (value === '' || value === null) callback(new Error('Повторите пароль'))
        else if (value !== formData.password && value !== null) callback(new Error('Пароли должны совпадать'))
        callback()
    }
    let validatePassword = (rule, value, callback) => {
        if (value === '' || value === null) callback(new Error('Введите пароль'))
        else if (value !== formData.password_confirmation && formData.password_confirmation !== undefined && formData.password_confirmation !== null) callback(new Error('Пароли должны совпадать'))
        callback()
    }

    let dates = (rule, value, callback) => {
        if(!value.length) callback(new Error('Выберите даты'))
        callback()
    }

    return {
        password: [
            {validator: validatePassword, trigger: 'blur'},
        ],
        password_confirmation: [
            {validator: validatePasswordConfirmed, trigger: 'blur'},
        ],
        email: [
            {required: true, message: 'Введите email', trigger: 'blur'},
            {type: 'email', message: 'Введите корректный email', trigger: 'blur'}
        ],
        name: [
            {required: true, message: 'Введите имя', trigger: 'blur'},
        ],
        valute_id: [
            {required: true, message: 'Выберите валюту', trigger: 'change'},
        ],
        dates: [
            {validator: dates, trigger: 'blur'}
        ]
    }

}
