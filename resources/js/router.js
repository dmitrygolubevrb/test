import Vue from "vue";
import VueRouter from "vue-router";


Vue.use(VueRouter)

const router = new VueRouter({
    mode: 'history',

    routes: [

        // Auth Routes
        {
            path: '/auth/login',
            component: () => import('./components/Auth/Login'),
            name: 'auth.login'
        },
        {
            path: '/auth/register',
            component: () => import('./components/Auth/Registration'),
            name: 'auth.register'
        },
        {
            path: '/auth/forgot-password',
            component: () => import('./components/Auth/ForgotPassword'),
            name: 'auth.forgot'
        },
        {
            path: '/auth/reset-password',
            component: () => import('./components/Auth/ResetPassword'),
            name: 'auth.reset-password'
        },
        {
            path: '/auth/email/verify',
            component: () => import('./components/Auth/VerifyEmail'),
            name: 'auth.verify-email'
        },


    ]
})

// router.beforeEach((to, from ,next) => {
//     const token = localStorage.getItem('xsrf_token')
//     if(!token){
//         if(to.name === 'auth.login' || to.name === 'auth.register') return next()
//         return next()
//     }
//     next()
// })

export default router
