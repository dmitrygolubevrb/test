import Model from "./Model";

export default class CurrencyQoutation extends Model {

    resource() {
        return 'currencies/quotations';
    }

}
