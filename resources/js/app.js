import Vue from "vue";
import router from "./router";
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import locale from 'element-ui/lib/locale/lang/ru-RU'
import Index from './components/Index';
import Auth from './components/Auth/Index'
import store from './store';
import {BootstrapVue} from "bootstrap-vue";
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import axios from "axios";
import {Model} from "vue-api-query";

Model.$http = axios

Vue.use(ElementUI, {locale})
Vue.use(BootstrapVue)
// Vue.config.silent = true
require('./bootstrap')

const app = new Vue({
    el: '#app',
    components: {
        Index,
        Auth,
    },
    router,
    store,
});

window.axios.interceptors.request.use(config => {
    store.dispatch('SET_IS_LOADING', true)
    return config
}, error => {
    store.dispatch('SET_IS_LOADING', false)
})

window.axios.interceptors.response.use(config => {
        store.dispatch('SET_IS_LOADING', false)
        return config
    }
    , error => {
        store.dispatch('SET_IS_LOADING', false)
        if (error.response.data.errors) error.response.data.message = error.response.data.errors[Object.keys(error.response.data.errors)[0]][0]
        if (error.response.status === 401 || error.response.status === 419 || error.response.status === 302) {
            window.location.replace('/auth/login')
        }
        return Promise.reject(error)
    })
