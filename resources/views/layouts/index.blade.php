<!doctype html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSS -->
    <link rel="stylesheet" href="{{ asset('js/app.js') }}">
    <title>TEST</title>
</head>
<body>
<div>
    <div class="container">
        <div id="app">
            <Index></Index>
        </div>
    </div>

    <!-- Fontawesome -->
    <script src="https://kit.fontawesome.com/0276f09879.js" crossorigin="anonymous"></script>
    <!-- JS -->
    <script src="{{ asset('js/app.js') }}"></script>

</div>
</body>
</html>





