<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return new \App\Http\Resources\Auth\UserResource($request->user());
});

Route::group(['namespace' => 'Currency', 'prefix' => 'currencies'], function () {
    Route::get('/', 'IndexController');
    Route::post('/', 'SyncController');
    Route::delete('/', 'DestroyController');
    Route::group(['namespace' => 'Quotation', 'prefix' => 'quotations'], function(){
       Route::get('/', 'IndexController');
    });
});

