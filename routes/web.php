<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::view('/auth/reset-password', 'layouts.auth')->name('password.reset');
Route::view('/auth/email/verify', 'layouts.auth')->name('verification.notice');
Route::view('/auth/login', 'layouts.auth')->name('login');
Route::view('/auth/{page}', 'layouts.auth')->where('page', '.*')->name('auth');
Route::view('/{page?}', 'layouts.index')->where('page', '.*')->middleware(['auth:sanctum']);
