<?php


namespace App\Services;


use App\Http\Requests\Currency\SyncRequest;
use App\Models\Currency;
use Illuminate\Support\Arr;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;


class CurrencyService
{
    public function sync(SyncRequest $request)
    {
        $existingsDates = DB::table('currencies')->select('date')->distinct()->get();
        $requiredDates = $this->getRequiredDates($request->days_count);
        $this->compareDates($requiredDates, $existingsDates);
        $data = $this->mergeResult($this->getResult($requiredDates));
        $this->save($data);
        return true;
    }


    private function getRequiredDates($daysCount): array
    {
        $requiredDates = [];
        $date = Carbon::now();
        $requiredDates[] = $date->isoFormat('DD/MM/YYYY');
        for($i = 1; $i < $daysCount; $i++){
            $requiredDates[] = $date->sub(1, 'day')->isoFormat('DD/MM/YYYY');
        }
        return $requiredDates;
    }

    private function compareDates(&$requiredDates, $existingsDates)
    {
        foreach ($existingsDates as $existingDate){
            if(in_array($existingDate->date, $requiredDates))
                unset($requiredDates[array_search($existingDate->date, $requiredDates)]);
        }
    }

    private function getXML($date)
    {
        return new \SimpleXMLIterator("http://www.cbr.ru/scripts/XML_daily.asp?date_req={$date}", null, true);
    }

    private function parseXML($xml, $date, &$result)
    {
        $dateQuotation = Arr::get(json_decode(json_encode($xml->attributes()), true), '@attributes.Date');
        $dateQuotationToFormat = Carbon::parse($dateQuotation)->isoFormat('DD/MM/YYYY');

        if(!isset($result[$dateQuotation]) && $dateQuotationToFormat === $date) {
            foreach (json_decode(json_encode($xml->children()), true)['Valute'] as $currency){
                $result[$dateQuotation][] = [
                    'valute_id' => Arr::get($currency, '@attributes.ID'),
                    'num_code' => $currency['NumCode'],
                    'сhar_code' => $currency['CharCode'],
                    'name' => $currency['Name'],
                    'value' => $currency['Value'],
                    'date' => Carbon::createFromFormat('d/m/Y', $dateQuotationToFormat)->isoFormat('YYYY/MM/DD'),
                ];
            }
        }
    }

    private function mergeResult($result): array
    {
        $data = [];
        array_map(function($currency) use (&$data) {
            $data = array_merge($data, $currency);
        }, $result);
        return $data;
    }

    private function getResult($requiredDates): array
    {
        $result = [];
        foreach ($requiredDates as $date){
            $xml = $this->getXML($date);
            $this->parseXML($xml, $date, $result);
        }
        return $result;
    }

    private function save($data)
    {
        foreach (collect($data)->chunk(200) as $chunk) {
            Currency::upsert($chunk->toArray(), []);
        }
    }
}
