<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Carbon;

class Currency extends Model
{
    use HasFactory;
    protected $fillable = ['valute_id', 'num_code', 'сhar_code', 'name', 'value', 'date'];
    protected $table = 'currencies';


    public function scopeFrom($query, $from)
    {
        return $query->whereDate('date', '>=', $from);
    }

    public function scopeTo($query, $to)
    {
        return $query->whereDate('date', '<=', $to);
    }

    public function scopeValute($query, $valuteId)
    {
        return $query->where('valute_id', '=', $valuteId);
    }

    public function getDateAttribute($value)
    {
        return Carbon::parse($value)->isoFormat('DD.MM.YYYY');
    }


}
