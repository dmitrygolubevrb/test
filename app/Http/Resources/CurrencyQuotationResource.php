<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CurrencyQuotationResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'valute_id' => $this->valute_id,
            'num_code' => $this->num_code,
            'name' => $this->name,
            'char_code' => $this->сhar_code,
            'value' => $this->value,
            'date' => $this->date
        ];
    }
}
