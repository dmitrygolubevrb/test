<?php

namespace App\Http\Controllers\Currency;

use App\Http\Controllers\Controller;
use App\Models\Currency;

class DestroyController extends Controller
{
    public function __invoke()
    {
        Currency::truncate();
        return response([]);
    }
}
