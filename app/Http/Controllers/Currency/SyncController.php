<?php

namespace App\Http\Controllers\Currency;

use App\Http\Controllers\Controller;
use App\Http\Requests\Currency\SyncRequest;
use App\Services\CurrencyService;

class SyncController extends Controller
{
    public function __invoke(SyncRequest $request, CurrencyService $currencyService)
    {
        $currencyService->sync($request);
        return response([]);
    }
}
