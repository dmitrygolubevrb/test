<?php

namespace App\Http\Controllers\Currency\Quotation;

use App\Http\Controllers\Controller;
use App\Http\Resources\CurrencyQuotationResource;
use App\Models\Currency;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

class IndexController extends Controller
{
    public function __invoke()
    {

        $currencyQuotations = QueryBuilder::for(Currency::class)
            ->allowedFilters([
                AllowedFilter::scope('from'),
                AllowedFilter::scope('to'),
                AllowedFilter::scope('valute'),
            ])
            ->get();
        return CurrencyQuotationResource::collection($currencyQuotations);
    }
}
