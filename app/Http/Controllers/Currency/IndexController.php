<?php

namespace App\Http\Controllers\Currency;

use App\Http\Controllers\Controller;
use App\Http\Resources\CurrencyResource;
use App\Models\Currency;
use App\Repositories\Interfaces\CurrencyRepositoryInterface;


class IndexController extends Controller
{
    public function __invoke(CurrencyRepositoryInterface $currencyRepository)
    {
        return CurrencyResource::collection($currencyRepository->getAll());
    }
}
